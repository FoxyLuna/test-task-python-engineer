# import of standard and third-party libraries
import sys, pickle, socket, os
import win32api, psutil

"""
The following variables collect information about the PC.
Variables return different value types.
"""
pc_name = win32api.GetComputerName()

logical_drives = win32api.GetLogicalDriveStrings()

local_time = win32api.GetLocalTime()

domain = win32api.GetDomainName()

"""
About os_version:
The return value's low word is the major/minor version of Windows. 
The high word is 0 if the platform is Windows NT, or 1 if Win32s on Windows 3.1
"""
os_version = win32api.GetVersion()

sys_time = win32api.GetSystemTime()

platform = sys.platform

# Information about CPU usage
cpu_info = [psutil.cpu_times(), psutil.cpu_stats(), psutil.cpu_freq()]
# Information about disks and memory usage
disk_info =[psutil.disk_partitions(), psutil.disk_usage('/'), psutil.disk_io_counters(perdisk=False)]
memory = psutil.virtual_memory()

ip_adress = socket.gethostbyname_ex(socket.gethostname())[2]

# Data packing and creating a binary file

data = [pc_name, logical_drives, local_time, domain,
        os_version, sys_time, platform,
        cpu_info, disk_info, ip_adress]

with open('userdata.pickle', 'wb') as f:
    pickle.dump(data, f)

# Creating unique name for each file and saving
filename = str(ip_adress)
os.rename('userdata.pickle', filename + '.pickle')