from django.db import models

class ClientInfo(models.Model):
    pc_name = models.CharField(max_length=50, null=True)
    logical_drivers = models.CharField(max_length=100, null=True)
    local_time = models.CharField(max_length=20, null=True)
    domain = models.CharField(max_length=50, null=True)
    os_version = models.CharField(max_length=20, null=True)
    sys_time = models.DateTimeField(null=True)
    platform = models.CharField(max_length=30, null=True)
    cpu_info = models.TextField(null=True)
    disk_info = models.TextField(null=True)
    ip_address = models.CharField(max_length=15, null=True)

    class Meta:
        ordering = ['pc_name']

class Company(models.Model):
    file = models.FileField(upload_to='temp/')

