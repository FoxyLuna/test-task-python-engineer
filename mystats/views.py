from django.shortcuts import render_to_response
from django.views.generic.base import View
from django.http import HttpResponseRedirect, HttpResponse
from django.core.mail import EmailMessage
from .forms import UploadFileForm

class HomeView(View):
    template_name = 'home.html'
    form_class = UploadFileForm

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render_to_response(self.template_name, {'form' : form})
    """
    Loading files in django uses models.
    The form is associated with these models.
    """
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            filef = form.save(commit=True) # saving file to DB
            # sending a message to e-mail
            email = EmailMessage('Data saved', 'Your data is saved successfully', to=['souseiseki30@gmail.com'])
            email.send()
            return HttpResponseRedirect('done/')
        return render_to_response(self.template_name, {'form' : form})

def Done(request):
    return HttpResponse('Done, check your e-mail!')