from django.test.testcases import TestCase
from mystats.models import ClientInfo
from mystats.forms import UploadFileForm

class ClientInfoTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        ClientInfo.objects.create(pc_name='Lion', domain='win32')
        ClientInfo.objects.create(pc_name='Roma', domain='cygwin', ip_address=['192.0.0.168'])

    def setUp(self):
        lion = ClientInfo.objects.get(pc_name='Lion')
        roma = ClientInfo.objects.get(pc_name='Roma')

    def test_false_is_false(self):
        print("Method: test_false_is_false.")
        self.assertFalse(False)

    def test_false_is_true(self):
        print("Method: test_false_is_true.")
        self.assertTrue(True)

    def test_pc_name_max_length(self):
        pc = ClientInfo.objects.get(pc_name='Roma')
        max_length = pc._meta.get_field('pc_name').max_length
        self.assertEquals(max_length, 50)

    def test_object_name(self):
        domain = ClientInfo.objects.get(domain='win32')
        self.assertTrue(domain)

    def tearDown(self):
        print('The model test passed successfully')

class UploadFormTestCase(TestCase):

    def test_renew_form_date_field_label(self):
        form = UploadFileForm()
        self.assertTrue(
            form.fields['file'].label == 'File')

    def test_renew_form_date_field_help_text(self):
        form = UploadFileForm()
        self.assertFalse(form.fields['file'].help_text, 'Upload your file.')

    def test_post_form(self):
        form = UploadFileForm()
        self.assertTrue(form)

    def test_empty_field(self):
        form = UploadFileForm()
        self.assertTrue(form.fields['file'])

