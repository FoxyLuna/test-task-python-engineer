from django.apps import AppConfig


class MystatsConfig(AppConfig):
    name = 'mystats'
