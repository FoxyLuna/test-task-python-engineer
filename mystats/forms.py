from django import forms
from .models import Company

class UploadFileForm(forms.ModelForm):

    class Meta:
        model = Company
        fields = ('file',)

