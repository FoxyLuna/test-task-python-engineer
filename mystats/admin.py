from django.contrib import admin
from mystats.models import Company, ClientInfo

class CompanyAdmin(admin.ModelAdmin):
    list_display = ('file',)

admin.site.register(Company, CompanyAdmin)

